
# coding: utf-8


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
import datetime
import itertools
import sklearn.model_selection
import argparse

parser=argparse.ArgumentParser(description='LBMP Model')
parser.add_argument('--gridsearch', type=bool, default=False)
parser.add_argument('--timestamp', type=str, default='2017-08-01 00:00')
args=parser.parse_args()







def date_parse(string):
    new_string=list(string)
    
    mon=new_string[0:2]
    mon=mon[0]+mon[1]
    day=new_string[3:5]
    day=day[0]+day[1]
    year=new_string[6:10]
    year=year[0]+year[1]+year[2]+year[3]
    hr=new_string[11:13]
    hr=hr[0]+hr[1]
    mint=new_string[14]+new_string[-1]
    
    
    return datetime.datetime(int(year),int(mon),int(day),int(hr),int(mint))




data=pd.read_csv('20170101damlbmp_zone_csv/20170101damlbmp_zone.csv',parse_dates=[0],date_parser=date_parse)





for i in range(1,13):
    if i is 1:
        for j in range(2,32):  #already have first day of the year
                if j < 10:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
    elif i is 2:
        for j in range(1,29):  
                if j < 10:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
    elif i is 9 or i is 4 or i is 6:
        for j in range(1,31):  
                if j < 10:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
    elif i is 11:
        for j in range(1,31):  
                if j < 10:
                    data=data.append(pd.read_csv('2017{}01damlbmp_zone_csv/2017{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('2017{}01damlbmp_zone_csv/2017{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
    elif i is 12 or i is 10:
        for j in range(1,32):  
                if j < 10:
                    data=data.append(pd.read_csv('2017{}01damlbmp_zone_csv/2017{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('2017{}01damlbmp_zone_csv/2017{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
    else:
        for j in range(1,32):  
                if j < 10:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}0{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
                else:
                    data=data.append(pd.read_csv('20170{}01damlbmp_zone_csv/20170{}{}damlbmp_zone.csv'.format(i,i,j),parse_dates=[0],date_parser=date_parse))
  
        
        
        
        
        





nycdata=data[data['Name']=='N.Y.C.']
nycdata=nycdata.set_index('Time Stamp')




plt.plot(nycdata['LBMP ($/MWHr)'][0:-1],label='LBMP')

plt.legend()
plt.xlabel('Time Stamp')
plt.ylabel('$/MWHr')
plt.title('LBMP for total dataset')
plt.show()
plt.close()
#the data from 12/28 onwards do not look like they represent the dataset as a whole, making a decision to drop that data as it will not help the model

nycdata=nycdata[:-100]





plt.plot(nycdata['LBMP ($/MWHr)'])
plt.legend()
plt.xlabel('Time Stamp')
plt.ylabel('$/MWHr')
plt.title('LBMP for dataset with last 100 entries removed')
plt.show()
plt.close()
#looks better




plt.plot(nycdata['LBMP ($/MWHr)'][0:100])
plt.legend()
plt.xlabel('Time Stamp')
plt.ylabel('$/MWHr')
plt.title('LBMP for first 100 entries')
plt.show()
plt.close()
#looks seasonal on a daily basis






#summary statistics for total data set
total_mean=np.mean(nycdata['LBMP ($/MWHr)'])
total_std=np.std(nycdata['LBMP ($/MWHr)'])




#subtract every LBMP from the mean and plot 
plt.plot(nycdata['LBMP ($/MWHr)']-total_mean)
plt.ylabel('$/MWHr')
plt.xlabel('Time Stamp')
plt.title('LBMP offset by mean')
plt.show()
plt.close()
#doesn't appear to be white noise





result=sm.tsa.seasonal_decompose(nycdata['LBMP ($/MWHr)'],model='multiplicative',freq=24*30)
#frequency as number of observations/month





plt.hist(result.resid,range=(0,3),bins=100)
plt.xlabel('Residuals')
plt.ylabel('Frequency')
plt.title('Histogram of Residuals of seasonal decomposition')
plt.show()
plt.close()
#looks normal, great!


result.plot()
plt.show()
plt.close()





#for grid search of model hyperparameters
p = d = q = range(0, 2)
pdq = list(itertools.product(p, d, q))
seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]





dat=nycdata['LBMP ($/MWHr)'].resample('H').mean()
if args.gridsearch:
    min_aic=1000000
    best_pdq=None
    best_spdq=None
    for param in pdq:
        for param_seasonal in seasonal_pdq:
            try:
                mod=sm.tsa.statespace.SARIMAX(y,order=param,seasonal_order=param_seasonal)
                results=mod.fit()
                if results.aic < min_aic:
                    best_pdq=param
                    best_spdq=param_seasonal
            except:
                continue
else:
    best_pdq=(1,1,1)
    best_spdq=(1,1,1,12)






model=sm.tsa.statespace.SARIMAX(dat,order=best_pdq,seasonal_order=best_spdq)





results=model.fit(disp=0)





results.plot_diagnostics(figsize=(16,8))
plt.show()
plt.close()
#not perfect but residuals are nearly normal




pred = results.get_prediction(start=pd.to_datetime('2017-08-01'),end=pd.to_datetime('2017-08-02') ,dynamic=False)





plt.plot(pred.predicted_mean,label=('Model Estimate'))
plt.plot(nycdata['LBMP ($/MWHr)']['2017-08-01'],label='True Value')
plt.xlabel('Time')
plt.ylabel('LBMP') 
plt.title('True Value and Model Estimate for August 1 2017')
plt.legend()
plt.show()
plt.close()





pred2= results.get_prediction(start=pd.to_datetime(args.timestamp),end=pd.to_datetime(args.timestamp) ,dynamic=False)


print('The predicted value for the given timestamp is {}'.format(pred2.predicted_mean[0]))

