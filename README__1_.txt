﻿The model can be run from the command line by being in the directory containing the CSV files and the Python Script and typing "Python blueprint_model.py"
There are two command line arguments to the model, one being gridsearch which takes a boolean value.  If true, multiple models will be fit over a grid of pdq and seasonal pdq values, with the parameters with the lowest AIC being selected to fit the model.
The other command line argument is timestamp which is a string with format "yyyy-mm-dd hh:mm" and will determine for which date and time the predicted value is output.
The default value for gridsearch is False and for timestamp is '2017-08-01 00:00'
The following packages are used:  pandas, matplotlib.pyplot, numpy, statsmodels.api, datetime, itertools, and argparse.
The model used is a seasonal ARIMA model with default values of (1,1,1) for pdq and (1,1,1,24) for seasonalpdq.  This model was chosen as ARIMA is a standard in the field but lacks support for a seasonal dataset and it seemed during exploration that the data was seasonal over the period of a day
The default hyperparameters were chosen as the parameters that minimized the AIC for the model, with 24 representing the fact that the data seems to be seasonal on the scale of a day and there are 24 observation per day


Insights and Conclusions
* The LBMP data from 12/28/17 onwards did not seem to represent the dataset as a whole, almost as if the values were shifted upwards by a significant constant.  As such, those dates were not used for further analysis/fitting the model
* When the LBMP data were plotted on the scale of a few days at a time, a daily seasonal pattern was observed.  Prices are low at the beginning and end of a day, have a local maximum in the morning hours, then dip again before reaching a maximum in the evening hours, and finally dip back down to a low value around midnight, restarting the cycle.
* The mean of the LBMP data was found and subtracted from all the data points and plotted as a check to see if the data was white noise.  This did not seem to be the case. 
* The data was decomposed into trend, seasonal, and residual components and it appeared as though the residuals were normally distributed, implying that the decomposition was successful.
* The seasonal arima model has good agreement with the observed values, but I’m not sure how well it would function as a forecaster for values that have not already been observed.
* Helpful additional data inputs would be values of LBMP for other years besides 2017.  This could help determine if there is a seasonal trend in the data on the timescale of months or seasons (e.g. fall, winter).  Since there is only data for one year, it is not possible to see if there is a seasonal trend on those timescales, as any differences between months could theoretically just be random noise without the added information of values for other years.
* The model could be improved by the implementation of training and testing sets, but I was not sure how to achieve this as it did not seem like the measurements were independent from one another and I did not know how to account for that.